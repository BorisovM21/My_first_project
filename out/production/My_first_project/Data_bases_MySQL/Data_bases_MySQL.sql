USE academy;

CREATE TABLE Passport
(
  id         INT         NOT NULL AUTO_INCREMENT,
  first_name VARCHAR(64) NOT NULL DEFAULT 'Unknown',
  last_name  VARCHAR(64) NOT NULL DEFAULT 'Unknown',
  PRIMARY KEY (id)
);
INSERT INTO passport (id, first_name, last_name)
VALUES
  (1, 'Алексей', 'Иванов'), (2, 'Петр', 'Бунин'), (3, 'Иван', 'Пупкин'),
  (4, 'Дмитрий', 'Гамов'), (5, 'Сергей', 'Усенко'), (6, 'Елена', 'Дружина'),
  (7, 'Виктория', 'Семенова'), (8, 'Анастасия', 'Бутко'), (9, 'Елизавета', 'Завьялова'),
  (10, 'Семен', 'Альтов'), (11, 'Вера', 'Глаголева'), (12, 'Петр', 'Вельяминов'),
  (13, 'Павел', 'Барановский'), (14, 'Леонид', 'Гаркуша'), (15, 'Виталий', 'Гармаш'),
  (16, 'Василий', 'Дудко'), (17, 'Иван', 'Помадоро'), (18, 'Екатерина', 'Куйбы'),
  (19, 'Анатолий', 'Петровский'), (20, 'Дарья', 'Пуха');

CREATE TABLE Students
(
  id          INT NOT NULL AUTO_INCREMENT,
  id_passport INT,
  data        DATE,
  PRIMARY KEY (id),
  FOREIGN KEY (id_passport) REFERENCES Passport (id)
);
INSERT INTO students (id, id_passport, data)
VALUES
  (1, 9, '2017-09-01'), (2, 10, '2016-09-01'), (3, 11, '2016-09-01'),
  (4, 12, '2016-09-01'), (5, 13, '2017-02-01'), (6, 14, '2014-09-01'),
  (7, 15, '2017-09-01'), (8, 16, '2017-02-01'), (9, 17, '2017-09-01'),
  (10, 18, '2016-09-01'), (11, 19, '2015-03-05'), (12, 20, '2017-09-01');

CREATE TABLE Teachers
(
  id          INT NOT NULL AUTO_INCREMENT,
  id_passport INT,
  PRIMARY KEY (id),
  FOREIGN KEY (id_passport) REFERENCES Passport (id)
);

INSERT INTO teachers (id, id_passport)
VALUES
  (1, 1), (2, 2), (3, 3), (4, 4), (5, 5),
  (6, 6), (7, 7), (8, 8);

CREATE TABLE forms
(
  id   INT         NOT NULL AUTO_INCREMENT,
  name VARCHAR(64) NOT NULL DEFAULT 'Unknown',
  PRIMARY KEY (id)
);
INSERT INTO forms (id, name)
VALUES
  (1, 'очная форма обучения'),
  (2, 'заочная форма обучения'),
  (3, 'вечерняя форма обучения');

CREATE TABLE groups
(
  id      INT NOT NULL AUTO_INCREMENT,
  name    VARCHAR(64),
  id_form INT,
  PRIMARY KEY (id),
  FOREIGN KEY (id_form) REFERENCES forms (id)
);
INSERT INTO groups (id, name, id_form)
VALUES
  (1, 'М-1', 1), (2, 'М-2', 1), (3, 'ЭП-1', 1), (4, 'ОБЗ-1', 1), (5, 'ЭП-3', 2),
  (6, 'УПП-2', 2), (7, 'УПП-1', 1), (8, 'М-3', 3), (9, 'ОБЗ-2', 2), (10, 'ПО', 3);

CREATE TABLE student_group
(
  id_student INT,
  id_group   INT,
  FOREIGN KEY (id_group) REFERENCES groups (id),
  FOREIGN KEY (id_student) REFERENCES Students (id)
);
INSERT INTO student_group (id_student, id_group)
VALUES
  (1, 1), (2, 2), (3, 3), (4, 4), (5, 5),
  (6, 6), (7, 7), (8, 8), (9, 9), (10, 10),
  (11, 1), (12, 2);

CREATE TABLE Subjects
(
  id   INT         NOT NULL AUTO_INCREMENT,
  name VARCHAR(64) NOT NULL DEFAULT 'Unknown',
  PRIMARY KEY (id)
);
INSERT INTO subjects (id, name)
VALUES
  (1, 'Украинский язык'), (2, 'Математика'), (3, 'Физика'), (4, 'Химия'), (5, 'Биология'),
  (6, 'Физическая подготовка'), (7, 'Политология'), (8, 'История'),
  (9, 'Астрономия'), (10, 'Русский язык'), (11, 'Английский язык'),
  (12, 'География'), (13, 'Информатика');

CREATE TABLE subject_teacher
(
  id_subject INT,
  id_teacher INT,
  FOREIGN KEY (id_subject) REFERENCES Subjects (id),
  FOREIGN KEY (id_teacher) REFERENCES Teachers (id)
);
INSERT INTO subject_teacher (id_subject, id_teacher)
VALUES
  (1, 1), (2, 2), (3, 3), (4, 4), (5, 5),
  (6, 6), (7, 7), (8, 8), (9, 1), (10, 2),
  (11, 2), (12, 5), (13, 2);

CREATE TABLE marks
(
  id         INT NOT NULL AUTO_INCREMENT,
  id_student INT,
  id_group   INT,
  id_subject INT,
  mark       INT(2),
  date       DATE,
  id_teacher INT,
  PRIMARY KEY (id),
  FOREIGN KEY (id_student) REFERENCES Students (id),
  FOREIGN KEY (id_group) REFERENCES groups (id),
  FOREIGN KEY (id_subject) REFERENCES Subjects (id),
  FOREIGN KEY (id_teacher) REFERENCES Teachers (id)
);
INSERT INTO marks (id, id_student, id_group, id_subject, mark, date, id_teacher)
VALUES
  (1, 1, 1, 1, 5, '2017-09-05', 1),  (2, 1, 1, 2, 4, '2017-09-05', 2),
  (3, 1, 1, 3, 4, '2017-09-03', 3),  (4, 1, 1, 4, 5, '2017-09-03', 4),
  (5, 1, 1, 5, 3, '2017-09-15', 5),  (6, 2, 2, 6, 5, '2017-09-15', 6),
  (7, 7, 2, 2, 4, '2017-09-25', 2),  (8, 2, 2, 8, 5, '2017-09-25', 8),
  (9, 9, 2, 2, 5, '2017-09-22', 2),  (10, 2, 2, 10, 3, '2017-09-22', 2),
  (11, 1, 3, 3, 5, '2017-09-05', 3), (12, 2, 3, 3, 4, '2017-09-05', 3),
  (13, 3, 3, 3, 3, '2017-09-03', 3), (14, 4, 3, 3, 4, '2017-09-03', 3),
  (15, 5, 3, 3, 4, '2017-09-15', 3), (16, 6, 4, 4, 3, '2017-09-15', 4),
  (17, 7, 4, 4, 4, '2017-09-25', 4), (18, 2, 4, 4, 5, '2017-09-25', 4),
  (19, 9, 4, 4, 2, '2017-09-22', 4), (20, 2, 4, 4, 5, '2017-09-22', 4),
  (21, 1, 5, 5, 5, '2017-09-05', 5), (22, 2, 5, 5, 5, '2017-09-05', 5),
  (23, 3, 5, 5, 4, '2017-09-03', 5), (24, 4, 5, 5, 4, '2017-09-03', 5),
  (25, 5, 5, 5, 4, '2017-09-15', 5), (26, 6, 6, 6, 2, '2017-09-15', 6),
  (27, 7, 6, 6, 5, '2017-09-25', 6), (28, 2, 6, 6, 3, '2017-09-25', 6),
  (29, 9, 6, 6, 3, '2017-09-22', 6), (30, 2, 6, 6, 3, '2017-09-22', 6),
  (31, 10, 7, 7, 5, '2017-09-05', 7), (32, 2, 7, 7, 5, '2017-09-05', 7),
  (33, 10, 7, 7, 4, '2017-09-03', 7), (34, 4, 7, 7, 5, '2017-09-03', 7),
  (35, 5, 7, 7, 4, '2017-09-15', 7), (36, 6, 8, 12, 5, '2017-09-15', 5),
  (37, 11, 8, 8, 5, '2017-09-25', 8), (38, 2, 8, 12, 4, '2017-09-25', 5),
  (39, 11, 8, 8, 3, '2017-09-22', 8), (40, 2, 8, 8, 4, '2017-09-22', 8),
  (41, 1, 9, 9, 4, '2017-09-05', 1), (42, 2, 2, 2, 5, '2017-09-05', 2),
  (43, 12, 9, 9, 2, '2017-09-03', 1), (44, 4, 4, 4, 3, '2017-09-03', 4),
  (45, 5, 9, 9, 4, '2017-09-15', 1), (46, 6, 10, 13, 4, '2017-09-15', 2),
  (47, 12, 10, 10, 5, '2017-09-25', 2), (48, 2, 10, 13, 5, '2017-09-25', 2),
  (49, 9, 10, 10, 5, '2017-09-22', 2), (50, 2, 10, 5, 5, '2017-09-22', 5),
  (51, 6, 10, 10, 5, '2017-09-25', 2), (52, 8, 10, 12, 5, '2017-09-25', 5),
  (53, 4, 10, 10, 5, '2017-09-25', 2), (54, 7, 10, 12, 5, '2017-09-25', 5);

SELECT Passport.first_name, Passport.last_name, Passport.id
FROM Passport, Teachers
WHERE
  Passport.id = Teachers.id;

SELECT Passport.first_name, Passport.last_name, Passport.id, Subjects.name FROM Passport
INNER JOIN Teachers
ON Passport.id = Teachers.id_passport
INNER JOIN Subjects
ON  Teachers.id = Subjects.id
INNER JOIN subject_teacher
ON Subjects.id = subject_teacher.id_subject
WHERE Subjects.name = 'Математика';

SELECT Passport.first_name, Passport.last_name, Passport.id
FROM Passport
INNER JOIN Students
ON Passport.id = Students.id_passport
INNER JOIN student_group
ON Students.id = student_group.id_student
INNER JOIN groups
ON student_group.id_group = groups.id
INNER JOIN forms
ON groups.id_form = forms.id
WHERE forms.name = 'вечерняя форма обучения';

SELECT Passport.first_name, Passport.last_name, Passport.id
FROM Passport
INNER JOIN Students
ON Passport.id = Students.id_passport
INNER JOIN marks
ON Students.id = marks.id_student
WHERE mark = 2;

SELECT COUNT(Passport.id)
FROM Passport
INNER JOIN Teachers
ON Passport.id = Teachers.id_passport;

SELECT COUNT(Passport.id)
FROM Passport
INNER JOIN Students
ON Passport.id = Students.id_passport
INNER JOIN student_group
ON Students.id = student_group.id_student
INNER JOIN groups
ON student_group.id_group = groups.id
WHERE name ='М-2';

SELECT COUNT(Passport.id)
FROM Passport
INNER JOIN Students
ON Passport.id = Students.id_passport
INNER JOIN student_group
ON Students.id = student_group.id_student
INNER JOIN groups
ON student_group.id_group = groups.id
INNER JOIN forms
ON groups.id_form = forms.id
WHERE forms.name = 'очная форма обучения';

SELECT AVG(marks.mark),groups.name
FROM marks
INNER JOIN groups
ON marks.id_group = groups.id
INNER JOIN student_group
ON groups.id = student_group.id_group
WHERE name = 'М-1';