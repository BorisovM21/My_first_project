package DZ_Recursion;

import java.util.Scanner;

public class DZ_Recursion {


    public static void main(String[] args) {
        test ();
        int numberUser = NumberUser();
        System.out.println(FirstNumber(numberUser));
    }
    private static int NumberUser() {
        int result;
        System.out.println("Введите любое число: ");
        Scanner input = new Scanner(System.in);
        result = input.nextInt();
        return result;
    }
    private static int FirstNumber(int numberUser) {
        if ((numberUser / 10) == 0) {
            return numberUser;
        }
        int factor = MultNumber(numberUser);
        int tempResult = (numberUser % 10) * factor;
        int rec = FirstNumber(numberUser / 10);
        int res = tempResult + rec;
        return res;
    }
    private static int LengthNumber(int number) {
        int lengthNumber = 0;
        while (number != 0) {
            lengthNumber++;
            number /= 10;
        }
        return lengthNumber;
    }
    private static int MultNumber(int number) {
        int sum = 1;
        int temp = LengthNumber(number);
        for (int i = 1; i < temp; i++) {
            sum *= 10;
        }
        return sum;
    }
    static void test (){
        int [] data = {12345, 11345, 745623, 71904, 333};
        int [] expectedResult = {54321, 54311, 326547, 40917, 333};
        boolean passed = true;
        for (int i = 0; i < data.length; i++) {
            int  temp = FirstNumber(data[i]);
            if(temp!=expectedResult[i]) {
                System.out.printf("Ошибка теста ", temp, expectedResult[i]);
                passed = false;

            }
        }
        if (passed) {
            System.out.println("Тест пройден");
        }
    }
}