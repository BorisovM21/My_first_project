package Post_Threads;

import Post_Threads.entities.PostNew;
import Post_Threads.entities.PostmanNew;
import Post_Threads.entities.SenderNew;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Thread.*;

public class MailMain {
    //private static final String ANSI_CYAN = "\u001B[36m";
    //private static final String ANSI_PURPLE = "\u001B[35m";

    private static final int KOL_SENDERS = 10; // Количество точек приема посылок
    private static final int KOL_POSTMEN = 2; // Количество почтальенов, которые разносят посылки
    private static final int KOL_MAX_SENDER_POSTMAN = 3; // Мах количество посылок, которые разносит почтальен
    private static final int POST_MAX = 20; // Количество посылок, которые могут уместиться на почте
    private static final int SLEEP_TIME = 5000; // Тайм-аут перед остановкой работы почты

    private static PostNew _postNew = new PostNew(POST_MAX);
    private static List<PostmanNew> _postmen = new ArrayList<>();
    private static List<SenderNew> _senders = new ArrayList<>();
    private static List<Thread> _postmenThreads = new ArrayList<>();
    private static List<Thread> _sendersThreads = new ArrayList<>();

    public static void main(String[] args) {
        createSenders();   // Создание точек приема посылок
        createPostmen();   // Создание почтальенов

        createListOfSendersThreads();   // Создание списка оприходования почтовых посылок
        createListOfPostmenThreads();   // Создание списка разноски почтовых посылок

        startSendersThreads();   // Старт получения посылок
        startPostmenThreads();   // Старт разноски посылок

        sleepMain(100);   // Остановка на время SLEEP_TIME

        interruptSendersThreads();   // Прерывание процесса оприходования посылок
        interruptPostmenThreads();   // Прерывание процесса разноски посылок

        printResults();   // Вывод результатов работы
    }

    // Печать результатов работы программы
    private static void printResults() {
        System.out.println("--------------------------------------");
        System.out.println("Количество принятых посылок " + _postNew.getParcelsNumberAccept());
        System.out.println("Количество выданных посылок " + _postNew.getParcelsNumberSend());
        System.out.println("Остаток посылок на почте " + _postNew.getParcelsNumber());
        System.out.println("--------------------------------------");
    }

    // Прерывание потоков разноски почтовых посылок
    private static void interruptPostmenThreads() {
        for (Thread thread : _postmenThreads) {
            thread.interrupt();
        }
    }

    // Прерывание потоков приемки почтовых посылок
    private static void interruptSendersThreads() {
        for (Thread thread : _sendersThreads) {
            thread.interrupt();
        }
    }

    // Погружение в сон основной части программы (прием и разноска почтовых посылок продолжается)
    private static void sleepMain(int i) {
        try {
            sleep(SLEEP_TIME);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // Старт потоков разноски почтовых посылок
    private static void startPostmenThreads() {
        for (Thread thread : _postmenThreads) {
            thread.start();
        }
    }

    // Старт потоков приема почтовых посылок
    private static void startSendersThreads() {
        for (Thread thread : _sendersThreads) {
            thread.start();
        }
    }

    // Создание списка разноски почтовых посылок
    private static void createListOfPostmenThreads() {
        for (PostmanNew postmanNew : _postmen) {
            _postmenThreads.add(new Thread(postmanNew));
        }
    }

    // Создание списка получения почтовых посылок
    private static void createListOfSendersThreads() {
        for (SenderNew senderNew : _senders) {
            _sendersThreads.add(new Thread(senderNew));
        }
    }

    // Создание списка почтальенов
    private static void createPostmen() {
        for (int i = 0; i < KOL_POSTMEN; i++) {
            _postmen.add(new PostmanNew(_postNew, KOL_MAX_SENDER_POSTMAN));
        }
    }

    // Создание списка точек приема почтовых посылок
    private static void createSenders() {
        for (int i = 0; i < KOL_SENDERS; i++) {
            _senders.add(new SenderNew(_postNew));
        }
    }
}
