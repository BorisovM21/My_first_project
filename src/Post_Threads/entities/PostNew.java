package Post_Threads.entities;


public class PostNew {
    private int _parcelsMAX;   // МАХ количество посылок (вместительность почты)
    private int _parcelsNumber;   // Количество доступных посылок на почте
    private int _parcelsNumberAccept;   // Количество принятых посылок
    private int _parcelsNumberSend;   // Количество выданных посылок
    private final Object _postWait = new Object();

    private static final int SLEEP_TIME_INPUT_SENDER = 100; // Тайм-аут перед обработкой поступившей посылки
    private static final int SLEEP_TIME_OUTPUT_POSTMAN = 100; // Тайм-аут перед обработкой выдаваемой посылки(ок)

    public PostNew(int parcelsMAX) {
        _parcelsMAX = parcelsMAX;
    }


    public int getParcelsMAX() {
        return _parcelsMAX;
    }


    public void setParcelsMAX(int parcelsMAX) {
        _parcelsMAX = parcelsMAX;
    }


    public int getParcelsNumber() {
        return _parcelsNumber;
    }


    public void setParcelsNumber(int parcelsNumber) {
        _parcelsNumber = parcelsNumber;
    }

    public int getParcelsNumberAccept() {
        return _parcelsNumberAccept;
    }

    public void setParcelsNumberAccept(int parcelsNumberAccept) {
        _parcelsNumberAccept = parcelsNumberAccept;
    }

    public int getParcelsNumberSend() {
        return _parcelsNumberSend;
    }

    public void setParcelsNumberSend(int parcelsNumberSend) {
        _parcelsNumberSend = parcelsNumberSend;
    }

    public Object getPostWait() {
        return _postWait;
    }

    // Принятие посылки для последующей обработки
    public void parcelAccept(int parcelsNumber) {
        synchronized (_postWait) {
            while (_parcelsNumber >= _parcelsMAX) {   // Количество посылок на почте не может быть больше МАХ
                try {
                    _postWait.wait(SLEEP_TIME_INPUT_SENDER);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            setParcelsNumber(_parcelsNumber + parcelsNumber); //Увеличение количества доступных посылок
            setParcelsNumberAccept(_parcelsNumberAccept + parcelsNumber); //Ув. количества принятых посылок ВСЕГО

            System.out.println("На почту поступила " + getParcelsNumberAccept() + " посылка(-ки/-ок).");

            _postWait.notifyAll();
        }
    }

    // Выдача посылки с почты для пазноски
    public void parcelGive(int parselsOfNumber) {
        int i = 0;
        synchronized (_postWait) {
            while (_parcelsNumber <= 0) {   // Ожидание появления посылок на почте для выдачи
                try {
                    _postWait.wait(SLEEP_TIME_OUTPUT_POSTMAN);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            setParcelsNumber(_parcelsNumber - parselsOfNumber); //Уменьшение количества доступных посылок
            setParcelsNumberSend(_parcelsNumberSend + parselsOfNumber); // Ув. количества отправленных посылок ВСЕГО
            i = _parcelsNumber + parselsOfNumber;
            System.out.println("С почты выдано " + parselsOfNumber + " из " + i + " посылка(-ки/-ок).");

            _postWait.notifyAll();
        }
    }
}

