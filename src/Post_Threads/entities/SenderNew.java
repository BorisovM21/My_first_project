package Post_Threads.entities;

/**
 * Created by mvs on 23.01.2018.
 */
public class SenderNew implements Runnable {

    private static final int SLEEP_TIME_MIN =  200;
    private static final int SLEEP_TIME_MAX = 1000;
    private static int _sendersNumber = 0;   // Текущий номер выданной посылки

    private int _senderNumberPointSend;   // Количество точек приема посылок
    private int _parcelsNumberInOneTime = 1;   // Количество посылок, которые принимают одновременно
    private int _parcelsNumberSent = 0;   //Количество выданных посылок
    private PostNew _postNew;

    // Конструктор - создание посылки
    public SenderNew(PostNew postNew) {
        _postNew = postNew;
    }

    public static int getSendersNumber() {
        return _sendersNumber;
    }

    public static void setSendersNumber(int sendersNumber) {
        _sendersNumber = sendersNumber;
    }

    public int getSenderNumberPointSend() {
        return _senderNumberPointSend;
    }

    public void setSenderNumberPointSend(int senderNumberPointSend) {
        _senderNumberPointSend = senderNumberPointSend;
    }

    // Получить количество посылок, которые принимают одновременно
    public int getParcelsNumberInOneTime() {
        return _parcelsNumberInOneTime;
    }

    // Установить количество посылок, которые принимают одновременно
    public void setParcelsNumberInOneTime(int parcelsNumberInOneTime) {
        _parcelsNumberInOneTime = parcelsNumberInOneTime;
    }

    // Получение количества доступных посылок
    public int getParcelsNumberSent() {
        return _parcelsNumberSent;
    }

    // Установка количества доступных посылок
    public int setParcelsNumberSent(int parcelsNumberSent) {
        _parcelsNumberSent = parcelsNumberSent;
        return parcelsNumberSent;
    }

    public PostNew getPostNew() {
        return _postNew;
    }

    public void setPostNew(PostNew postNew) {
        _postNew = postNew;
    }

    @Override
    public void run() {
        int i = 0;
        while (true) {
            // Если кол-во посылок меньше МАХ загрузки почты
            if (_postNew.getParcelsNumber() < _postNew.getParcelsMAX()) {

                _postNew.parcelAccept(getParcelsNumberInOneTime());
                setParcelsNumberSent(getParcelsNumberSent() + 1);
                //System.out.println("На почту в окно " + getParcelsNumberSent() + " поступила " + getParcelsNumberSent() + " посылка.");
//                System.out.println("На почту поступила " + getSendersNumber() + "   " + getSenderNumberPointSend() + " " + i + " посылка.");
                try {
                    Thread.sleep(timeRandomSleep());
                } catch (InterruptedException e) {
                    break;
                }
            } else { //Спать, пока не освободится место на почте
                try {
                    Thread.sleep(SLEEP_TIME_MIN);
                } catch (InterruptedException ignored) {
                    //ignore
                }
            }
        }
    }

    private long timeRandomSleep() {
        //return SLEEP_TIME_MIN + (long) (Math.random() * SLEEP_TIME_MAX);
        return 4000;
    }

    @Override
    public String toString() {
        return " " +
                "_senderNumberPointSend=" + _senderNumberPointSend;
    }
}
