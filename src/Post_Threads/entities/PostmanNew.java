package Post_Threads.entities;

public class PostmanNew implements Runnable {

    private static final long SLEEP_TIME_MIN  =  200;
    private static final long SLEEP_TIME_MAX  = 1000;
    private static final long SLEEP_TIME_WAIT =  300;

    private static int _postmanNumber = 0;
    private static int _postmanNumberPointSend;
    private int _postmanCarrying = 1;
    private PostNew _postNew;

    // Конструктор - создание почтальена
    public PostmanNew(PostNew postNew) {
        _postNew = postNew;
    }

    // В конструктор передается количество посылок, которые одновременно может переносить почтальен
    public PostmanNew(PostNew postNew, int postmanCarrying) {
        _postNew = postNew;
        _postmanCarrying = postmanCarrying;
        setPostmenNumber(getPostmanNumber() + 1);
    }

    // Получить количество доступных почтальенов
    public static int getPostmanNumber() {
        return _postmanNumber;
    }

    // Установить количество доступных почтальенов
    public static void setPostmenNumber(int postmenNumber) {
        _postmanNumber = postmenNumber;
    }

    //
    public static int getPostmanNumberPointSend() {
        return _postmanNumberPointSend;
    }

    //
    public static void setPostmanNumberPointSend(int postmanNumberPointSend) {
        _postmanNumberPointSend = postmanNumberPointSend;
    }

    // Получить количество почтальенов "в дороге"
    public int getPostmanCarrying() {
        return _postmanCarrying;
    }

    // Установить количество почтальенов "в дороге"
    public void setPostmanCarrying(int postmanCarrying) {
        _postmanCarrying = postmanCarrying;
    }

    //
    public PostNew getPostNew() {
        return _postNew;
    }

    //
    public void setPostNew(PostNew postNew) {
        _postNew = postNew;
    }

    @Override
    public void run() {
        while (true) {
            if (_postNew.getParcelsNumber() > 0) {  //
                // Если количество посылок > 0
                if (_postNew.getParcelsNumber() >= getPostmanCarrying()) {
                    // Если количество посылок >= "грузоподъемности" почтальена
                    _postNew.parcelGive(getPostmanCarrying());
                } else {
                    // Иначе переносит 1 посылку
                    _postNew.parcelGive(_postNew.getParcelsNumber());
                }
                try {
                    Thread.sleep(timeRandomSleep());
                } catch (InterruptedException e) {
                    break;
                }
            } else {
                try {
                    Thread.sleep(SLEEP_TIME_WAIT);
                } catch (InterruptedException ignored) {
                    //ignore
                }
            }
        }
    }

    private long timeRandomSleep() {
        return SLEEP_TIME_MIN + (long) (Math.random() * SLEEP_TIME_MAX);
    }

    @Override
    public String toString() {
        return " " +
                PostmanNew.getPostmanNumber() +
                "_postmanNumberPointSend=" + _postmanNumberPointSend +
                ", _postmanCarrying=" + _postmanCarrying +
                ", _postNew=" + _postNew +
                '}';
    }
}
