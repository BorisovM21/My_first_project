package Data_bases_MySQL;

import java.sql.*;

class Connection_Data_base_MySQL {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        String userName = "root";
        String password = "1111";
        String connectionUrl = "jdbc:mysql://localhost:3306";
        Class.forName("com.mysql.jdbc.Driver");
        //class Connection - конект к базе даннных
        try (Connection connection = DriverManager.getConnection(connectionUrl, userName, password);
             Statement statement = connection.createStatement()) {
            //class Statement - операторы SQL
            //Юзаем базу, дропаем таблицу,создаем заново таблицу
            statement.executeUpdate("USE data_base_car;");
            statement.executeUpdate("DROP TABLE  car_number;");
            statement.executeUpdate("CREATE TABLE  car_number(id_car_number INT NOT NULL AUTO_INCREMENT," +
                    "  serial_befo_number  VARCHAR(64) NOT NULL DEFAULT 'AA',\n" +
                    "  _number             INT         NOT NULL DEFAULT 0000,\n" +
                    "  serial_after_number VARCHAR(64) NOT NULL DEFAULT 'AA',\n" +
                    "  id_car              INT,\n" +
                    "  PRIMARY KEY (id_car_number),\n" +
                    "  FOREIGN KEY (id_car) REFERENCES car (id_car))");
            // добавление данных в таблицу
            statement.executeUpdate("INSERT INTO car_number SET _number = (5412)," +
                    " serial_befo_number = ('AS')," +
                    " serial_after_number = ('EE') ");
            statement.executeUpdate("INSERT INTO car_number SET _number = (4421)," +
                    "  serial_befo_number = ('AD')," +
                    " serial_after_number = ('AE')");
            // достаем данные с таблицы
            ResultSet resultSet = statement.executeQuery("SELECT * FROM car_number");
            while (resultSet.next()) {
                System.out.println("id : " + resultSet.getString(1));
                System.out.println(resultSet.getString(2));
                System.out.println(resultSet.getInt(3));
                System.out.println(resultSet.getString(4));
                System.out.println("------------------");
            }
            ResultSet resultSet1 = statement.executeQuery("SELECT _number FROM car_number WHERE id_car_number = 1");
            while (resultSet1.next()) {
                System.out.println("_number : " + resultSet1.getString(1));
            }
        }
    }
}
