package AccrualOfScholarshipsToStudents;

import java.io.*;
import java.util.Arrays;

public class Student implements Serializable {
    private String lastName;
    private String name;
    private String middleName;
    private String numGroup;
    private int[] marks = new int[5];
    private int category;


    Student(String lastName, String name, String middleName, String numGroup, int[] marks) {
        setLastName(lastName);
        setName(name);
        setMiddleName(middleName);
        setNumGroup(numGroup);
        if (isMarksValid(marks)) {
            this.marks = marks.clone();
        }
    }

    private boolean isMarksValid(int[] marks) {
        if (marks.length < 3 || marks.length > 5) {
            System.err.println("Количество предметов должно быть от 3 до 5");
            return false;
        }
        for (int i = 0; i < marks.length; i++) {
            if (marks[i] < 0 || marks[i] > 5) {
                System.err.println("Оценка должны быть от 0 до 5");
                return false;
            }
        }
        return true;
    }


    private void setMarks(int indexMasiva, int mark) {
        if (indexMasiva > marks.length) {
            System.err.println("Error, wrong subject");
        }
        if (mark > 5 || mark < 0) {
            System.err.println("Error, wrong mark");
        }

    }

    public int[] getMarks() {
        return marks.clone();
    }

    public int getCategory() {
        return category;
    }

    private void setCategory(int category) {
        this.category = category;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getNumGroup() {
        return numGroup;
    }

    public void setNumGroup(String numGroup) {
        this.numGroup = numGroup;
    }

    public static void printStudens(Student[] student) {

        System.out.println("Имя\t\t\tФамилия\t\tВозраст\t\tнадбанка к степендии");
        System.out.println("-----------------------------------------");
        for (int i = 0; i < student.length; ++i) {
            System.out.printf("%s\t\t%s\t\t%s\t\t%d%n",
                    student[i].getLastName(),
                    student[i].getName(),
                    student[i].getMiddleName(),
                    student[i].getCategory());
        }
    }

    static void sortStudents(Student[] students) {
        int n = students.length;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (students[j].getLastName().compareTo(students[j - 1].getLastName()) < 0) {
                    Student temp = students[j - 1];
                    students[j - 1] = students[j];
                    students[j] = temp;
                }
            }
        }
        setCategoryForAllStudents(students);
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (students[j].getCategory() > students[j - 1].getCategory()) {
                    Student temp = students[j - 1];
                    students[j - 1] = students[j];
                    students[j] = temp;
                }
            }
        }
    }

    private static void setCategoryForAllStudents(Student[] students) {
        for (int i = 0; i < students.length; i++) {
            students[i].calculateCategory();
        }
    }

    private void calculateCategory() {
        int result = 0;
        setCategory(result);
        if (isAllExamsPast() && isMoreThenTwoSatisfactorily()) {
            int temp = 0;
            int bestMarks = 0;
            for (int i = 0; i < marks.length; i++) {
                temp += marks[i];
                bestMarks += 5;
                result = temp * 100 / bestMarks;
            }
            if (result == 100) {
                setCategory(100);
            }
            if (result < 100 && result >= 50) {
                setCategory(25);
            }
            if (isOneSatisfactorily() && result < 100 && result >= 50) {
                setCategory(50);
            }
        }

    }

    private boolean isAllExamsPast() {
        for (int i = 0; i < marks.length; i++) {
            if (marks[i] == 0) {
                return false;
            }
        }
        return true;
    }

    private boolean isMoreThenTwoSatisfactorily() {
        int counter = 0;
        for (int i = 0; i < marks.length; i++) {
            if (marks[i] < 4) {
                counter++;
                if (counter >= 2) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public String toString() {
        return "Student{" +
                "lastName='" + lastName + '\'' +
                ", name='" + name + '\'' +
                ", middleName='" + middleName + '\'' +
                ", numGroup='" + numGroup + '\'' +
                ", marks=" + Arrays.toString(marks) +
                ", category=" + category +
                '}';
    }

    private boolean isOneSatisfactorily() {
        for (int i = 0; i < marks.length; i++) {
            if (marks[i] < 4) {
                return false;
            }
        }
        return true;
    }

    static void creatWriteAndRead(Student student) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("studentFile.txt"))) {
            oos.writeObject(student);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("studentFile.txt"))) {
            try {
                student = (Student) ois.readObject();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            System.out.println(student);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}