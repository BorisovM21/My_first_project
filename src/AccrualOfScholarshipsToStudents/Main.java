package AccrualOfScholarshipsToStudents;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Student Vasay = new Student("Пупкин", "Вася", "Иванович",
                "М2017", new int[]{5, 5, 5, 4, 3});
        Student Ylia = new Student("Веткин", "Илья", "Артемович",
                "М2017", new int[]{5, 4, 5, 4, 5});
        Student Vika = new Student("Кутина", "Вика", "Васильевна",
                "М2017", new int[]{4, 4, 4, 4, 5});
        Student Vova = new Student("Зиль", "Вова", "Егорович",
                "М2017", new int[]{4, 4, 4, 4, 5});
        Student Kolia = new Student("Старшый", "Николай", "Васильевич",
                "М2017", new int[]{5, 5, 5, 4, 5});
        Student Sasha = new Student("Корнев", "Саша", "Николаевич",
                "М2017", new int[]{5, 5, 5, 5, 5});
        Student Lena = new Student("Смирнова", "Елена", "Васильевна",
                "М2017", new int[]{4, 4, 5, 5, 5});
        Student Lyuda = new Student("Нелепова", "Людмила", "Васильевна",
                "М2017", new int[]{3, 3, 5, 5, 5});
        Student Olay = new Student("Миря", "Ольга", "Васильевна",
                "М2017", new int[]{5, 0, 5, 5, 5});

        Student[] groupM2017 = {Vasay, Ylia, Vika, Vova, Kolia, Sasha, Lena, Lyuda, Olay};

        Student.sortStudents(groupM2017);
        Student.printStudens(groupM2017);

    }
}