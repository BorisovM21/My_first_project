package _01_Threads;

import java.util.Scanner;

public class _01_Threads {
    public static void main(String[] args) throws InterruptedException {
        int number_threads = 0;

        Thread T1 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.printf(Thread.currentThread().getName());
                try {
                    while (true) {
                        Thread.sleep(100);
                    }
                } catch (InterruptedException ignored) {
                }
            }
        }, "Поток №1 запустился.\n");

        Thread T2 = new Thread(() -> {
            System.out.printf(Thread.currentThread().getName());
            try {
                while (true) {
                    Thread.sleep(100);
                }
            } catch (InterruptedException ignored) {
            }
        }, "Поток №2 запустился.\n");

        Thread T3 = new Thread(() -> {
            System.out.printf(Thread.currentThread().getName());
            try {
                while (true) {
                    Thread.sleep(100);
                }
            } catch (InterruptedException ignored) {
            }
        }, "Поток №3 запустился.\n");

        Thread T4 = new Thread(() -> {
            System.out.printf(Thread.currentThread().getName());
            try {
                while (true) {
                    Thread.sleep(100);
                }
            } catch (InterruptedException ignored) {
            }
        }, "Поток №4 запустился.\n");

        Thread T5 = new Thread(() -> {
            System.out.printf(Thread.currentThread().getName());
            try {
                while (true) {
                    Thread.sleep(100);
                }
            } catch (InterruptedException ignored) {
            }
        }, "Поток №5 запустился.\n");

        T1.start();
        number_threads = number_threads + 1;
        T2.start();
        number_threads = number_threads + 1;
        T3.start();
        number_threads = number_threads + 1;
        T4.start();
        number_threads = number_threads + 1;
        T5.start();
        number_threads = number_threads + 1;

        Scanner sc = new Scanner(System.in);
        while (number_threads > 0) {
            Thread.sleep(200);
            System.out.print("Введите номер потока: ");
            if (sc.hasNextInt() == true) {
                switch (sc.nextInt()) {
                    case 1:
                        T1.interrupt();
                        System.out.println("Поток № 1 завершился!");
                        number_threads = number_threads - 1;
                        break;
                    case 2:
                        T2.interrupt();
                        System.out.println("Поток № 2 завершился!");
                        number_threads = number_threads - 1;
                        break;
                    case 3:
                        T3.interrupt();
                        System.out.println("Поток № 3 закрыт!");
                        number_threads = number_threads - 1;
                        break;
                    case 4:
                        T4.interrupt();
                        System.out.println("Поток № 4 завершился!");
                        number_threads = number_threads - 1;
                        break;
                    case 5:
                        T5.interrupt();
                        System.out.println("Поток № 5 завершился!");
                        number_threads = number_threads - 1;
                        break;
                    default:
                        System.out.println("Необходимо ввести целое число от 1 до 5.");
                        break;
                }
            }
        }
    }
}