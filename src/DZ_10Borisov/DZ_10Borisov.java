package DZ_10Borisov;


import java.util.Arrays;
import java.util.Scanner;

public class DZ_10Borisov {

    public static void main(String[] args) {
        final int[] arr = new int[4];
        final int num;
        final String str;

        dataInput(arr); // запрос ввода даных у юзера
        insertionSorting(arr); // сортировка вставками и пошаговый вывод на экран этой сортировки
        sortingBubble(arr); // сортировка пузырьком и пошаговый вывод ее на экран
        selectionSorting(arr); // сортировка выборкой и пошаговый вывод ее на экран
        System.out.println("Конец програмы!");
    } // методы написаны ниже


    public static void dataInput(int[] ints) {// запрос ввода даных у юзера


        Scanner input = new Scanner(System.in);
        for (int i = 0; i < ints.length; i++) {
            System.out.print("Введите число: ");
            ints[i] = input.nextInt();
        }
        System.out.println(Arrays.toString(ints));

    }

    public static void insertionSorting(int[] ints) { // сортировка вставками и пошаговый вывод на экран этой сортировки

        System.out.println("Сортировка вставками");

        for (int i = 1; i < ints.length; i++) {
            int tmp = ints[i];
            int j;
            for (j = i - 1; j >= 0 && ints[j] < tmp; j--) {
                ints[j + 1] = ints[j];
                System.out.println(Arrays.toString(ints));
            }
            ints[j + 1] = tmp;
        }
        System.out.println(Arrays.toString(ints));
    }

    public static void sortingBubble(int[] ints) { // сортировка пузырьком и пошаговый вывод ее на экран
        System.out.println("Сортировка пузырьком");

        int tmp;
        for (int i = 0; i < ints.length; i++) {
            for (int j = 0; j < ints.length - 1; j++) {
                if (ints[j] > ints[j + 1]) {
                    tmp = ints[j];
                    ints[j] = ints[j + 1];
                    ints[j + 1] = tmp;
                    System.out.println(Arrays.toString(ints));
                }
            }
        }
    }

    public static void selectionSorting(int[] ints) { // сортировка выборкой и пошаговый вывод ее на экран
        System.out.println("Сортировка выборкой");
        int tmp1;
        int i = 0;
        int j = ints.length - 1;
        while (j > i) {
            tmp1 = ints[j];
            ints[j] = ints[i];
            ints[i] = tmp1;
            j--;
            i++;
            System.out.println(Arrays.toString(ints));
        }
    }
}