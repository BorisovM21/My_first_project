package MyLinkedList.list;

import java.util.Iterator;
import java.util.NoSuchElementException;


public class MyLinkedList implements Iterable<Integer> {
    private Element _head;
    private Element _tail;
    private long _size;

    public MyLinkedList() {
        _head = null;
        _tail = null;
        _size = 0;
    }

    /**
     * Добавляет данные в конец списка
     *
     * @param data значение для добавления
     */
    public void addToTail(int data) {
        Element newElement = new Element(data);
        if (_size == 0) {
            addToEmptyList(newElement);
        } else {
            _tail.next = newElement;
            newElement.prev = _tail;
            _tail = newElement;
            _size++;
        }
    }

    private void addToEmptyList(Element newElement) {
        newElement.next = null;
        newElement.prev = null;
        _tail = newElement;
        _head = newElement;
        _size++;
    }

    public void add(int data, long index) {
        Element newElement = new Element(data);
        if (index < 0 || index > _size) {
            printIndexOutOfBoundsMessage();
            return;
        }
        if (_size == 0) {
            addToEmptyList(newElement);
            return;
        }
        if (index == 0) {
            addToHead(data);
            return;
        }
        if (index == _size) {
            addToTail(data);
            return;
        }

        Element prevElement = getElement(index - 1);
        Element nextElement = prevElement.next;
        prevElement.next = newElement;
        newElement.prev = prevElement;
        nextElement.prev = newElement;
        newElement.next = nextElement;
        _size++;
    }

    private void printIndexOutOfBoundsMessage() {
        System.err.println("Выход за границу списка");
    }

    public void addToHead(int data) {
        Element newElement = new Element(data);
        if (_size == 0) {
            addToEmptyList(newElement);
        } else {
            _head.prev = newElement;
            newElement.next = _head;
            _head = newElement;
            _size++;
        }
    }

    public int get(long index) {
        if (isIndexInvalid(index)) {
            printIndexOutOfBoundsMessage();
            return Integer.MIN_VALUE;
        }
        return getElement(index).content;
    }

    private boolean isIndexInvalid(long index) {
        return index < 0 || index >= _size;
    }

    public void replace(int newData, long index) {
        if (index < 0 || index > _size) {
            printIndexOutOfBoundsMessage();
        }
        getElement(index).content = newData;

    }

    public void deleteFromHead() {
        if (_head != null) {
            _head = _head.next;
            _head.prev = null;
            _size--;


        }

    }

    public void deleteFromTail() {
        if (_tail != null)
        _tail = _tail.prev;
        _tail.next = null;
        _size--;

    }

    public void delete(long index) {
        if (index < 0 || index > _size) {
            printIndexOutOfBoundsMessage();
            return;
        }
        if (index == 0){
            deleteFromHead();
            return;
        }
        if (index == _size){
            deleteFromTail();
            return;
        }
        Element prevElement =getElement(index-1);
        Element nextElement = prevElement.next.next;
        prevElement.next = nextElement;
        nextElement.prev = prevElement;
        _size--;
    }

    public long size() {
        return _size;
    }

    private Element getElement(long index) {
        long middle = _size / 2;
        if (index <= middle) {
            return searchFromHead(index);
        }

        return searchFromTail(index);
    }

    private Element searchFromTail(long index) {
        Element result = _tail;
        long steps = _size - 1 - index;
        for (long i = 0; i < steps; ++i) {
            result = result.prev;
        }

        return result;
    }

    private Element searchFromHead(long index) {
        Element result = _head;
        for (long i = 0; i < index; ++i) {
            result = result.next;
        }

        return result;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("[");
        Element currentElement = _head;
        for (int i = 0; i < _size - 1; ++i) {
            stringBuilder.append(currentElement.content).append(", ");
            currentElement = currentElement.next;
        }
        if (currentElement != null) {
            stringBuilder.append(currentElement.content);
        }
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public Iterator<Integer> iterator() {
        //Создаем итератор на базе текущего списка
        return new MyLinkedListIterator(this);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private static class Element {
        Element prev;
        int content;
        Element next;

        Element(int content) {
            this.content = content;
        }
    }

    private static class MyLinkedListIterator implements Iterator<Integer> {

        private MyLinkedList _myLinkedList;
        private Element _cursor = null;

        MyLinkedListIterator(MyLinkedList myLinkedList) {
            _myLinkedList = myLinkedList;
        }

        @Override
        public boolean hasNext() {
            System.out.println("hasNext");
            if (_cursor == null) {
                return _myLinkedList._size > 0 /*? true : false*/;
            }

            return _cursor.next != null;
        }

        @Override
        public Integer next() {
            System.out.println("next");
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            //1 Move cursor to the next position
            if (_cursor == null) {
                _cursor = _myLinkedList._head;
            } else {
                _cursor = _cursor.next;
            }

            //2 Get content from the current element
            return _cursor.content;
        }
    }
}
