package MyLinkedList;

import java.util.Iterator;

import MyLinkedList.list.MyLinkedList;

public class ListMain {

    public static void main(String[] args) {
//        testAddToTail();
//        testAddIncorrectIndex();
//        testAddIndexBounds();
//        testAddInside();
//        testAddTOHead();
//        testDeleteFromTail();
//        testDeleteByIndex();

        MyLinkedList myLinkedList = new MyLinkedList();
        myLinkedList.addToTail(1);
        myLinkedList.addToTail(2);
        myLinkedList.addToTail(3);
        myLinkedList.addToTail(4);
        myLinkedList.addToTail(5);
        myLinkedList.addToTail(6);
        myLinkedList.addToTail(7);

        //Do this:
        //Step 1: Get iterator
        //  Iterator<Integer> iterator = myLinkedList.iterator();
        //Step 2: Run while loop
        // while (iterator.hasNext()) {
        //     System.out.println(iterator.next());
        // }
        //System.out.println("-----------");
        //Цикл for each - выполняет тоже самое что и конструкция выше
        for (Integer i : myLinkedList) {
            System.out.println(i);
        }

    }

    private static MyLinkedList testDeleteByIndex() {
        MyLinkedList myLinkedList = new MyLinkedList();
        myLinkedList.addToTail(1);
        myLinkedList.addToTail(2);
        myLinkedList.addToTail(3);
        myLinkedList.addToTail(4);
        myLinkedList.addToTail(5);
        myLinkedList.addToTail(6);
        myLinkedList.addToTail(7);
        myLinkedList.delete(3);
        return myLinkedList;
    }

    private static MyLinkedList testDeleteFromTail() {
        MyLinkedList myLinkedList = new MyLinkedList();
        myLinkedList.addToTail(1);
        myLinkedList.addToTail(2);
        myLinkedList.addToTail(3);
        myLinkedList.addToTail(4);
        myLinkedList.addToTail(5);
        myLinkedList.addToTail(6);
        myLinkedList.addToTail(7);
        myLinkedList.deleteFromTail();
        return myLinkedList;
    }

    private static MyLinkedList testAddTOHead() {
        MyLinkedList myLinkedList = new MyLinkedList();
        myLinkedList.addToTail(1);
        myLinkedList.addToTail(2);
        myLinkedList.addToTail(3);
        myLinkedList.addToTail(4);
        myLinkedList.addToTail(5);
        myLinkedList.addToTail(6);
        myLinkedList.addToTail(7);
        myLinkedList.addToHead(25);
        return myLinkedList;
    }

    private static void testAddInside() {
        MyLinkedList myLinkedList = new MyLinkedList();
        System.out.println(myLinkedList);
        myLinkedList.addToTail(1);
        myLinkedList.addToTail(2);
        myLinkedList.addToTail(3);
        myLinkedList.addToTail(4);
        myLinkedList.addToTail(5);
        myLinkedList.addToTail(6);
        myLinkedList.addToTail(7);
        myLinkedList.add(10, 1);
        myLinkedList.add(20, 7);
        System.out.println(myLinkedList);
        System.out.println("Done testAddInside");
    }

    /**
     * Тестирую add с граничными значениеями индексов
     */
    private static void testAddIndexBounds() {
        MyLinkedList myLinkedList = new MyLinkedList();
        myLinkedList.add(0, 0);
        myLinkedList.add(2, 1);
        System.out.println("Done testAddIndexBounds");

    }

    private static void testAddIncorrectIndex() {
        MyLinkedList myLinkedList = new MyLinkedList();
        myLinkedList.add(0, -1);
        myLinkedList.add(0, 1);
        System.out.println("Done testAddIncorrectIndex");
    }

    private static void testAddToTail() {
        MyLinkedList myLinkedList = new MyLinkedList();
        myLinkedList.addToTail(1);
        myLinkedList.addToTail(2);
        myLinkedList.addToTail(3);
        myLinkedList.addToTail(4);
        System.out.println("Done testAddToTail");
    }
}
