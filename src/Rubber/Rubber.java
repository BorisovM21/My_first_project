package Rubber;

import java.util.Arrays;
import java.util.EmptyStackException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class Rubber implements Iterable<String> {
    static private final int CHANGE_LENGTH = 5;
    private String[] strings;


    Rubber() {
        this.strings = new String[CHANGE_LENGTH];
    }

    public String[] getStrings() {
        return strings.clone();
    }

    public void setStrings(String[] strings) {
        if (strings == null) {
            this.strings = new String[CHANGE_LENGTH];
        }
        this.strings = strings.clone();
    }

    public void addString(String addedStrings) {
        for (int i = 0; i < strings.length; i++) {
            if (strings[i] == null) {
                strings[i] = addedStrings;
                return;
            }
        }
        String[] temp = new String[strings.length + CHANGE_LENGTH];
        for (int i = 0; i < strings.length; i++) {
            temp[i] = strings[i];
        }
        temp[strings.length] = addedStrings;
        strings = temp;

    }

    public void deleteStrings(int index) throws MyException {
        for (int i = index; i < strings.length - 1; i++) {
            strings[index] = strings[index + 1];
        }
        strings[strings.length - 1] = null;
        arrayReduction();
        if (index < 0 || index > strings.length) {
            throw new MyException("Индекс выхоидт за пределы массива");
        }

    }

    private void arrayReduction() {
        int counter = 0;
        for (int i = strings.length - 5; i < strings.length; i++) {
            if (strings[i] == null) {
                counter++;
            }
        }
        if (counter == 5) {
            String[] temp = new String[strings.length - CHANGE_LENGTH];
            for (int i = 0; i < temp.length; i++) {
                temp[i] = strings[i];
            }
            strings = temp;
        }
    }

    public void changeStringValue(int index, String newStrings) {
        if (index >= strings.length) {
            System.out.println("Индекс выхоидт за пределы массива");
            return;
        }
        strings[index] = newStrings;
    }

    public void moveStrings(int whatNeed, int whereNeed) {
        if (whereNeed > strings.length) {
            String[] temp = new String[whereNeed + 1];
            for (int i = 0; i < strings.length; i++) {
                temp[i] = strings[i];
            }
            strings = temp;
        }
        String temp;
        temp = strings[whereNeed];
        strings[whereNeed] = strings[whatNeed];
        strings[whatNeed] = temp;
    }

    @Override

    public String toString() {
        return "Rubber{" +
                "name=" + Arrays.toString(strings) +
                '}';
    }


    @Override
    public Iterator iterator() {
        return new MyRubberIterator(this);
    }

    private class MyRubberIterator implements Iterator {

        private Rubber rubber;
        private int index = 0;


        public MyRubberIterator(Rubber rubber) {
            this.rubber = rubber;
        }


        @Override
        public boolean hasNext() {
            System.out.println("hasNext");
            if (index < strings.length) {
                return true;
            }

            return false;
        }


        @Override
        public Object next() {

            return strings[index++];
        }
    }

    static class MyException extends Exception {

        public MyException(String massage) {
            super(massage);
        }

        public MyException(String massage, Throwable throwable) {
            super(massage, throwable);

        }

    }
}
