package Rubber;

import java.util.Iterator;

public class MainRubber {
    public static void main(String[] args) {
        Rubber rubber = new Rubber();

        rubber.addString("firs");
        rubber.addString("second");
        rubber.addString("next");
        try {
            rubber.deleteStrings(2);
        } catch (Rubber.MyException e) {
            e.printStackTrace();
        }
        rubber.changeStringValue(3, "new");
        System.out.println(rubber);
        rubber.changeStringValue(11, "1234sdf");


        Iterator<String> iterator = rubber.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        System.out.println("-----------");

        for (String i : rubber) {
            System.out.println(i);

        }
    }


}