package DZ_9;

import java.util.Arrays;
import java.util.Scanner;


public class DZ_9 {
    public static void main(String[] args) {
        int[] arr = new int[4];
        int num;
        String str;

        Scanner input = new Scanner(System.in);
        for (int i = 0; i < arr.length; i++) {
            System.out.print("Введите число: ");
            arr[i] = input.nextInt();
        }
        System.out.println("Сортировка вставками");

        for (int i = 1; i < arr.length; i++) {
            int tmp = arr[i];
            int j;
            for (j = i - 1; j >= 0 && arr[j] < tmp; j--) {
                arr[j + 1] = arr[j];
                System.out.println(Arrays.toString(arr));
            }
            arr[j + 1] = tmp;
        }
        System.out.println(Arrays.toString(arr));
        System.out.println("Сортировка пузырьком");

        int tmp;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                    System.out.println(Arrays.toString(arr));
                }
            }
        }
        System.out.println("Сортировка выборкой");
        int tmp1;
        int i = 0;
        int j = arr.length - 1;
        while (j > i) {
            tmp1 = arr[j];
            arr[j] = arr[i];
            arr[i] = tmp1;
            j--;
            i++;
            System.out.println(Arrays.toString(arr));
        }
        System.out.println("Конец програмы!");
    }
}