package MassOfPeopleInTheGroup;

import java.util.Scanner;
public class MassOfPeopleInTheGroup {


    public static void main(String[] args) {

        int[] quantityPeople = dataFromUser();
        if (peopleWeight(quantityPeople)) {
            System.out.println("Средний вес полных людей " + averageAmountFedPeople(quantityPeople));
            System.out.println("Средний вес обычных людей " + averageAmountOtherPeople(quantityPeople));
        }
    }
    private static int preservingTheWeightOfAllPeople() {
        int result;
        Scanner s = new Scanner(System.in);
        result = s.nextInt();
        return result;
    }
    private static int[] dataFromUser() {
        System.out.println("Введите количество людей в группе: ");
        int[] array = new int[preservingTheWeightOfAllPeople()];
        System.out.println("Введите массу каждого человека в группе: ");
        for (int i = 0; i < array.length; i++) {
            array[i] = preservingTheWeightOfAllPeople();
        }
        return array;
    }
    private static boolean peopleWeight(int[] quantityPeople) {
        boolean result = true;
        int count = 0;
        for (int i = 0; i < quantityPeople.length; i++) {
            if (quantityPeople[i] > 100) {
                count++;
            }
        }
        if (count <= 0) {
            System.err.println("Вес одного человека в группе должен превышать 100 кг");
            result = false;
        }
        return result;
    }
    private static int averageAmountFedPeople(int[] quantityPeople) {
        int count = 0;
        int averageAmount = 0;

        for (int i = 0; i < quantityPeople.length; i++) {
            if (quantityPeople[i] >= 100) {

                averageAmount += quantityPeople[i];
                count++;
            }
        }
        return averageAmount / count;
    }
    private static int averageAmountOtherPeople(int[] quantityPeople) {
        int averageAmount = 0;
        int count = 0;
        for (int i = 0; i < quantityPeople.length; i++) {
            if (quantityPeople[i] < 100) {
                count++;
                averageAmount += quantityPeople[i];
            }
        }
        if (averageAmount == 0) {
            return 0;
        }
        return averageAmount / count;
    }
}