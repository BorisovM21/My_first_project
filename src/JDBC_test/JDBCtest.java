package JDBC_test;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JDBCtest {
    public static void main(String[] args) {

        Connection connection = null;
        //URL к базе состоит из протокола:подпротокола://[хоста]:[порта_СУБД]/[БД] и других_сведений
        String url = "jdbc:mysql://localhost:3306";
        //Имя пользователя БД
        String name = "root";
        //Пароль
        String password = "1111";
        try {
            //Загружаем драйвер
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Драйвер подключен");
            //Создаём соединение
            connection = DriverManager.getConnection(url, name, password);
            System.out.println("Соединение установлено");
            //Для использования SQL запросов существуют 3 типа объектов:
            //1.Statement: используется для простых случаев без параметров
            Statement statement = null;
            statement = connection.createStatement();
            statement.executeUpdate("USE data_base_car;");
            //Выполним запрос
            ResultSet result1 = statement.executeQuery("SELECT * FROM car_number");
            //result это указатель на первую строку с выборки
            //чтобы вывести данные мы будем использовать
            //метод next() , с помощью которого переходим к следующему элементу
            System.out.println("Выводим statement");
            while (result1.next()) {
                System.out.println("id : " + result1.getString(1));
                System.out.println(result1.getString(2));
                System.out.println(result1.getInt(3));
                System.out.println(result1.getString(4));
                System.out.println("------------------");
            }

        } catch (Exception ex) {
            //выводим наиболее значимые сообщения
            Logger.getLogger(JDBCtest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
